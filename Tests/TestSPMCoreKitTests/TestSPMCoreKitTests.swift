    import XCTest
    @testable import TestSPMCoreKit

    final class TestSPMCoreKitTests: XCTestCase {
        func testExample() {
            // This is an example of a functional test case.
            // Use XCTAssert and related functions to verify your tests produce the correct
            // results.
            XCTAssertEqual(TestSPMCoreKit().text, "Hello, World!")
        }
    }
